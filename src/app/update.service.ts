import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { SwUpdate } from "@angular/service-worker";

@Injectable({
    providedIn: 'root'
})
export class UpdateService {
    constructor(private swupdate: SwUpdate, private snackbar: MatSnackBar) {
        if(!swupdate.isEnabled) {
            console.log('Service Worker is not enabled');
        }
        this.checkForUpdate();
    }

    public checkForUpdate(): void {
        this.swupdate.available.subscribe(() => this.promptUser());
    }

    private promptUser(): void {
        let snackbar = this.snackbar.open('An Update is Available', 'Reload', {
            duration: 10 * 1000 // 10 seconds
        });
        snackbar.onAction().subscribe(() => {
            this.swupdate.activateUpdate()
                .then(() => window.location.reload());
        })

    }
}